# README #

Mail Observer - 2015 
Developed: Max J. Rodríguez Beltrán - maxjrb[at]openitsinaloa.tk
Web: www.openitsinaloa.tk

Mail Observer a simple client to check unread emails

Mail Observer released under GPL V3

Mail Observer use AE.Net.Mail check project on https://github.com/andyedinborough/aenetmail under MIT License http://andyedinborough.mit-license.org/
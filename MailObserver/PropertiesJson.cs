﻿/*This file is part of Mail Observer
    2015 Max J. Rodríguez Beltran maxjrb[at]openitsnaloa.tk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System.Data;

namespace MailObserver
{
    public class PropertiesJson
    {
        public DataTable Accounts { get; set; }

        public int TimeLapse { get; set; }

        public int TimeNotify { get; set; }

        public string Location { get; set; }

        public bool WindowBorder { get; set; }

        public PropertiesJson()
        {
            Accounts = new DataTable();
            TimeLapse = 120000;
            TimeNotify = 5;
            Location = "RightUp";
            WindowBorder = false;
        }
    }
}

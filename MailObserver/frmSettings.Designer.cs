﻿namespace MailObserver
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.numTime = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numTimeNotify = new System.Windows.Forms.NumericUpDown();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbRightDown = new System.Windows.Forms.RadioButton();
            this.rbRightUp = new System.Windows.Forms.RadioButton();
            this.rbLeftDown = new System.Windows.Forms.RadioButton();
            this.rbLeftUp = new System.Windows.Forms.RadioButton();
            this.ckBorder = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeNotify)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // numTime
            // 
            this.numTime.Location = new System.Drawing.Point(203, 14);
            this.numTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTime.Name = "numTime";
            this.numTime.Size = new System.Drawing.Size(75, 21);
            this.numTime.TabIndex = 3;
            this.numTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Time to refresh domains:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Time for notification messages:";
            this.toolTip1.SetToolTip(this.label2, "Time in seconds");
            // 
            // numTimeNotify
            // 
            this.numTimeNotify.Location = new System.Drawing.Point(203, 44);
            this.numTimeNotify.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numTimeNotify.Name = "numTimeNotify";
            this.numTimeNotify.Size = new System.Drawing.Size(75, 21);
            this.numTimeNotify.TabIndex = 5;
            this.toolTip1.SetToolTip(this.numTimeNotify, "Time in seconds (min 5 seconds)");
            this.numTimeNotify.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbRightDown);
            this.groupBox1.Controls.Add(this.rbRightUp);
            this.groupBox1.Controls.Add(this.rbLeftDown);
            this.groupBox1.Controls.Add(this.rbLeftUp);
            this.groupBox1.Location = new System.Drawing.Point(12, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 69);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Notify Location";
            // 
            // rbRightDown
            // 
            this.rbRightDown.AutoSize = true;
            this.rbRightDown.Location = new System.Drawing.Point(165, 46);
            this.rbRightDown.Name = "rbRightDown";
            this.rbRightDown.Size = new System.Drawing.Size(89, 19);
            this.rbRightDown.TabIndex = 3;
            this.rbRightDown.TabStop = true;
            this.rbRightDown.Text = "Right Down";
            this.rbRightDown.UseVisualStyleBackColor = true;
            this.rbRightDown.CheckedChanged += new System.EventHandler(this.rbRightDown_CheckedChanged);
            // 
            // rbRightUp
            // 
            this.rbRightUp.AutoSize = true;
            this.rbRightUp.Location = new System.Drawing.Point(165, 21);
            this.rbRightUp.Name = "rbRightUp";
            this.rbRightUp.Size = new System.Drawing.Size(73, 19);
            this.rbRightUp.TabIndex = 2;
            this.rbRightUp.TabStop = true;
            this.rbRightUp.Text = "Right Up";
            this.rbRightUp.UseVisualStyleBackColor = true;
            this.rbRightUp.CheckedChanged += new System.EventHandler(this.rbRightUp_CheckedChanged);
            // 
            // rbLeftDown
            // 
            this.rbLeftDown.AutoSize = true;
            this.rbLeftDown.Location = new System.Drawing.Point(16, 46);
            this.rbLeftDown.Name = "rbLeftDown";
            this.rbLeftDown.Size = new System.Drawing.Size(80, 19);
            this.rbLeftDown.TabIndex = 1;
            this.rbLeftDown.TabStop = true;
            this.rbLeftDown.Text = "Left Down";
            this.rbLeftDown.UseVisualStyleBackColor = true;
            this.rbLeftDown.CheckedChanged += new System.EventHandler(this.rbLeftDown_CheckedChanged);
            // 
            // rbLeftUp
            // 
            this.rbLeftUp.AutoSize = true;
            this.rbLeftUp.Location = new System.Drawing.Point(16, 21);
            this.rbLeftUp.Name = "rbLeftUp";
            this.rbLeftUp.Size = new System.Drawing.Size(64, 19);
            this.rbLeftUp.TabIndex = 0;
            this.rbLeftUp.TabStop = true;
            this.rbLeftUp.Text = "Left Up";
            this.rbLeftUp.UseVisualStyleBackColor = true;
            this.rbLeftUp.CheckedChanged += new System.EventHandler(this.rbLeftUp_CheckedChanged);
            // 
            // ckBorder
            // 
            this.ckBorder.AutoSize = true;
            this.ckBorder.Location = new System.Drawing.Point(19, 165);
            this.ckBorder.Name = "ckBorder";
            this.ckBorder.Size = new System.Drawing.Size(141, 19);
            this.ckBorder.TabIndex = 9;
            this.ckBorder.Text = "Show window border";
            this.ckBorder.UseVisualStyleBackColor = true;
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 196);
            this.Controls.Add(this.ckBorder);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.numTimeNotify);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numTime);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSettings_FormClosing);
            this.Load += new System.EventHandler(this.frmSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeNotify)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numTimeNotify;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbRightDown;
        private System.Windows.Forms.RadioButton rbRightUp;
        private System.Windows.Forms.RadioButton rbLeftDown;
        private System.Windows.Forms.RadioButton rbLeftUp;
        private System.Windows.Forms.CheckBox ckBorder;
    }
}
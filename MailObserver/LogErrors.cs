﻿using System;
using System.IO;
using System.Windows.Forms;

namespace MailObserver
{
    class LogErrors
    {
        const string fileName = "ErrorLog.txt";

        static readonly string path = Path.GetDirectoryName(Application.ExecutablePath);

        static readonly string pathString = Path.Combine(path, fileName);

        public static void LogError(string errorLog)
        {

            if (!File.Exists(pathString)) //No File? Create
            {
                try
                {
                    using (var fs = new FileStream(pathString, FileMode.Create))
                    {
                        fs.Close();
                    }
                }
                catch (Exception)
                {
                    throw;
                }

            }

            if (File.ReadAllBytes(pathString).Length >= 100 * 1024 * 1024) // (100mB) File to big? Create new
            {
                try
                {
                    File.WriteAllText(pathString, String.Empty);
                }
                catch (Exception)
                {

                    throw;
                }

                using (var f = new StreamWriter(pathString, true))
                {
                    f.WriteLine("\r\n ======================");
                    f.WriteLine("Fecha hora: " + DateTime.Now.ToString());
                    f.WriteLine("\r\n ");
                    f.WriteLine(errorLog);
                    f.WriteLine("\r\n ======================");
                    f.Close();
                }
            }
            else
            {
                try
                {
                    using (var f = new StreamWriter(pathString, true))
                    {
                        f.WriteLine("\r\n ======================");
                        f.WriteLine("Fecha hora: " + DateTime.Now.ToString());
                        f.WriteLine("\r\n ");
                        f.WriteLine(errorLog);
                        f.WriteLine("\r\n ======================");
                        f.Close();
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }
}

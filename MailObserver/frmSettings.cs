﻿/*This file is part of Mail Observer
    2015 Max J. Rodríguez Beltran maxjrb[at]openitsnaloa.tk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Windows.Forms;

namespace MailObserver
{
    public partial class frmSettings : Form
    {
        public PropertiesJson prinproperties = new PropertiesJson();

        public frmSettings( PropertiesJson _properties)
        {
            prinproperties = _properties;
            InitializeComponent();
        }

        private void frmSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            prinproperties.WindowBorder = ckBorder.Checked;
            prinproperties.TimeLapse = (int)numTime.Value * 60000;
            frmMain.timeNotify = (int)numTimeNotify.Value;
            this.DialogResult = DialogResult.OK;
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            numTimeNotify.Value = frmMain.timeNotify;
            numTime.Value = (decimal) prinproperties.TimeLapse/60000;
            switch (prinproperties.Location)
            {
                case "LeftUp":
                    rbLeftUp.Checked = true;
                    break;
                case "LeftDown":
                    rbLeftDown.Checked = true;
                    break;
                case "RightUp":
                    rbRightUp.Checked = true;
                    break;
                case "RightDown":
                    rbRightDown.Checked = true;
                    break;
            }

            ckBorder.Checked = prinproperties.WindowBorder;
        }

        private void rbLeftUp_CheckedChanged(object sender, EventArgs e)
        {
            prinproperties.Location = "LeftUp";
        }

        private void rbLeftDown_CheckedChanged(object sender, EventArgs e)
        {
            prinproperties.Location = "LeftDown";
        }

        private void rbRightUp_CheckedChanged(object sender, EventArgs e)
        {
            prinproperties.Location = "RightUp";
        }

        private void rbRightDown_CheckedChanged(object sender, EventArgs e)
        {
            prinproperties.Location = "RightDown";
        }
    }
}

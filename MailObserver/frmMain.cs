﻿/*This file is part of Mail Observer
    2015 Max J. Rodríguez Beltran maxjrb[at]openitsnaloa.tk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AE.Net.Mail;
using Newtonsoft.Json;

namespace MailObserver
{
    public partial class frmMain : Form
    {
        public static int timeNotify { get; set; }

        public frmMain()
        {
            timeNotify = 5;
            InitializeComponent();
        }

        private DataTable _dtAccounts = new DataTable();
        private bool _isEditing = false;
        private bool _isNew = false;
        private List<object> _list =  new List<object>(); 
        private PropertiesJson properties;

        private const string fileName = "prop.json";
        private static readonly string path = Path.GetDirectoryName(Application.ExecutablePath);
        private readonly string pathString = Path.Combine(path, fileName);

        private void frmMain_Load(object sender, EventArgs e)
        {
            _dtAccounts.Columns.Add("Username");
            _dtAccounts.Columns.Add("Password");
            _dtAccounts.Columns.Add("Host");
            _dtAccounts.Columns.Add("Port");
            _dtAccounts.Columns.Add("SSL");
            _dtAccounts.Columns.Add("ShowSubject");
            _dtAccounts.Rows.Clear();

            loadGrid();

            dgAccounts.DataSource = _dtAccounts;
            dgAccounts.Refresh();

            if (dgAccounts.Columns.Count <= 0) return;
            dgAccounts.Columns[1].Visible = false;
        }

        private void loadGrid()
        {
            try
            {
                properties =
                    (PropertiesJson)
                        JsonConvert.DeserializeObject(File.ReadAllText(pathString), (typeof (PropertiesJson)));
                _dtAccounts = properties.Accounts;
                timer1.Interval = properties.TimeLapse;
                timeNotify = properties.TimeNotify;

            }
            catch (Exception ex)
            {
                LogErrors.LogError(ex.Message + ex.StackTrace);
                properties = new PropertiesJson();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text.Length <= 0 || txtPassword.Text.Length <= 0 || txtHost.Text.Length <= 0 ||
                txtPort.Text.Length <= 0)
            {
                MessageBox.Show("Check empty fields...", "Empty Fields", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var cifrado = Encryption.cifrarTexto(txtPassword.Text.Trim(), "thats right my horse", "H3yHeYH0l@", "SHA512", 2,
    "@1029384756*[]/#", 256);
                _dtAccounts.Rows.Add(txtUsername.Text.Trim(), cifrado, txtHost.Text.Trim(),
                    txtPort.Text.Trim(), chkSsl.Checked ? 1 : 0, chkSubject.Checked ? 1 : 0);
                dgAccounts.Refresh();
            }

            _isEditing = false;
            _isNew = false;
            tabControl2.SelectedIndex = 0;
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            tabControl2.SelectedIndex = _isEditing ? 1 : 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (_isEditing && !_isNew)
            {
                _dtAccounts.Rows.Add(_list[0].ToString(), _list[1].ToString(), _list[2].ToString(),
                    _list[3].ToString(), _list[4].ToString(), _list[5].ToString());
            }
            _isEditing = false;
            _isNew = false;

            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtHost.Text = string.Empty;
            txtPort.Text = string.Empty;
            chkSsl.Checked = false;

            tabControl2.SelectedIndex = 0;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            properties.Accounts = _dtAccounts;
            properties.TimeLapse = timer1.Interval;
            properties.TimeNotify = timeNotify;

            var jsonp = JsonConvert.SerializeObject(properties);

            try
            {
                File.WriteAllText(pathString, jsonp);
            }
            catch (Exception ex)
            {
                LogErrors.LogError(ex.Message + ex.StackTrace);
                Console.WriteLine("File Error..., check file permission.");
            }
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            _isEditing = true;
            _isNew = true;

            tabControl2.SelectedIndex = 1;

            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtHost.Text = string.Empty;
            txtPort.Text = string.Empty;
            chkSsl.Checked = false;
            chkSubject.Checked = false;
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            if (dgAccounts.RowCount <= 0) return;
            if (dgAccounts.CurrentRow == null) return;
            if (dgAccounts.CurrentRow.Index < 0) return;
            _dtAccounts.Rows.RemoveAt(dgAccounts.CurrentRow.Index);
            dgAccounts.Refresh();
        }

        private void btnEdit_Click_1(object sender, EventArgs e)
        {
            if (dgAccounts.RowCount <= 0) return;

            if (dgAccounts.CurrentRow == null) return;

            _isEditing = true;

            tabControl2.SelectedIndex = 1;

            _list = _dtAccounts.Rows[dgAccounts.CurrentRow.Index].ItemArray.ToList();

            txtUsername.Text = _list[0].ToString();
            txtPassword.Text = Encryption.descifrarTextoAES(_list[1].ToString(), "thats right my horse",
                    "H3yHeYH0l@", "SHA512", 2, "@1029384756*[]/#", 256); ;
            txtHost.Text = _list[2].ToString();
            txtPort.Text = _list[3].ToString();

            switch (_list[4].ToString())
            {
                case "0":
                    chkSsl.Checked = false;
                    break;
                case "1":
                    chkSsl.Checked = true;
                    break;
            }
            switch (_list[5].ToString())
            {
                case "0":
                    chkSubject.Checked = false;
                    break;
                case "1":
                    chkSubject.Checked = true;
                    break;
            }
            _dtAccounts.Rows.RemoveAt(dgAccounts.CurrentRow.Index);
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized != this.WindowState) return;
            notifyIcon.Visible = true;
            this.ShowInTaskbar = true;
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            checkMail();
        }

        private void checkMail()
        {
            var totalMails = 0;
            var listmessages = new List<string>();
            var isError = false;

            foreach (DataRow row in _dtAccounts.Rows)
            {
                var numEmail = 0;
                totalMails = 0;
                var username = row.ItemArray[0].ToString();
                var password = Encryption.descifrarTextoAES(row.ItemArray[1].ToString(), "thats right my horse",
                    "H3yHeYH0l@", "SHA512", 2, "@1029384756*[]/#", 256);
                var host = row.ItemArray[2].ToString();
                var port = Convert.ToInt32(row.ItemArray[3].ToString());
                var ssl = row.ItemArray[4].ToString() == "1";

                try
                {
                    using (var imap = new ImapClient(host, username, password, AuthMethods.Login, port, ssl))
                    {
                        imap.SelectMailbox("Inbox");

                        var msgs = imap.SearchMessages(
                            SearchCondition.Undeleted().And(SearchCondition.Unseen()), true, false);

                        totalMails = msgs.Length;

                        //Show Subject true 1 : 0
                        if (row.ItemArray[5].ToString() == "1")
                        {
                            listmessages.Add(totalMails.ToString() + " Unread messages from [" + username + "]" + Environment.NewLine);
                            foreach (var mail in msgs)
                            {
                                if (numEmail > 2)
                                {
                                    break;
                                }

                                listmessages.Add("[" + username + "] " + mail.Value.Subject.ToString() +
                                                 Environment.NewLine + " ");
                                numEmail++;
                            }
                        }
                        else
                        {
                            listmessages.Add(totalMails.ToString() + " Unread messages from [" + username + "]");
                            listmessages.Add("=========");
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogErrors.LogError(ex.Message + ex.StackTrace);
                    isError = true;
                }
            }
            
            if (isError)
            {
                lblStatus.Text = "Update Error, check account info...";
            }
            else
            {
                lblStatus.Text = "Updated info...";
            }

            var frmMessage = new frmMessage(listmessages, properties);
            frmMessage.Show();
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            using (var frmSet = new frmSettings(properties))
            {
                frmSet.ShowDialog();
                if (frmSet.DialogResult == DialogResult.OK)
                {
                    properties.TimeLapse = frmSet.prinproperties.TimeLapse;
                    timer1.Interval = properties.TimeLapse;
                    timer1.Enabled = false;
                    timer1.Enabled = true;
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("Mail Observer a simple client to check unread mails\r\n\n" +
                            "Developed by Max Rodríguez (2015) Released under GNU GPL v3.\r\n\n " +
                            "Check Source: https://bitbucket.org/Jaxmetalmax/mailobserver" + Environment.NewLine
                            + "Mail Observer use AE.Net.Mail (check README).",
                            "Mail Observer.",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            checkMail();
        }       
    }
}

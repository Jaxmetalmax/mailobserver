﻿/*This file is part of Mail Observer
    2015 Max J. Rodríguez Beltran maxjrb[at]openitsnaloa.tk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace MailObserver
{
    public partial class frmMessage : Form
    {
        private readonly List<string> mailMessage = new List<string>();
        private readonly int timeNotify = 0;
        private readonly PropertiesJson prinProperties;

        public frmMessage(List<string> _mailMessage, PropertiesJson _prinProperties)
        {
            InitializeComponent();
            mailMessage = _mailMessage;
            timeNotify = frmMain.timeNotify;
            prinProperties = _prinProperties;
        }

        private void frmMessage_Load(object sender, EventArgs e)
        {
            int y = 0;
            timer1.Interval = timeNotify*1000;
            timer1.Enabled = true;
            switch (prinProperties.Location)
            {
                case "RightUp":
                    y = Screen.PrimaryScreen.Bounds.Top + 30;
                    this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Width, y);
                    break;
                case "RightDown":
                    y = Screen.PrimaryScreen.Bounds.Bottom - (this.Height + 30);
                    this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Width, y);
                    break;
                case "LeftUp":
                    y = Screen.PrimaryScreen.Bounds.Top + 30;
                    this.Location = new Point(Screen.PrimaryScreen.Bounds.Left, y);
                    break;
                case "LeftDown":
                    y = Screen.PrimaryScreen.Bounds.Bottom - (this.Height + 30);
                    this.Location = new Point(Screen.PrimaryScreen.Bounds.Left, y);
                    break;
            }

            if (prinProperties.WindowBorder)
                this.FormBorderStyle = FormBorderStyle.FixedDialog;

            this.TopMost = true;

            foreach (var message in mailMessage)
            {
                listBox1.Items.Add((message));
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
